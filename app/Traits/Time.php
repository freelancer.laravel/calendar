<?php

namespace App\Traits;

use Carbon\Carbon;

trait Time{

    public static function isIntervalHours(string $date, $intervalHours): bool
    {
        $diff = Carbon::parse($date)->getTimestamp() - Carbon::now()->getTimestamp();

        $hours = $diff / 3600;

        return $hours > $intervalHours;
    }

    public static function endDate(string $date): bool
    {
        return Carbon::parse($date)->isPast();
    }
}
