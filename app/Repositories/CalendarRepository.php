<?php

namespace App\Repositories;

use App\Models\Calendar;
use Carbon\Carbon;

class CalendarRepository
{
    public function get($user, $request){

        $date_start = $request->get('date_start');

        $date_end = $request->get('date_end');

        return $user->calendars()->when($date_start, function ($query, $date_start) {
                $query->where('date_start', '>=', $date_start);
            })->when($date_end, function ($query, $date_end) {
                $query->where('date_end', '<=', $date_end);
            })->get();
    }

    public function create($user, $request){

        $dataValid = $request->validated();

        $date_end = Carbon::parse($dataValid['date_start'])
            ->addMinutes($dataValid['duration'])
            ->format('Y-m-d H:i');

        return Calendar::create(array_merge(
            $dataValid,
            ['user_id' => $user->id],
            ['date_end' => $date_end]
        ));
    }
    public function update($user, $request, $calendar){

        $dataValid = $request->validated();

        $date_end = Carbon::parse($dataValid['date_start'])
            ->addMinutes($dataValid['duration'])
            ->format('Y-m-d H:i');

        $calendar ->update(array_merge(
            $dataValid,
            ['date_end' => $date_end]
        ));
    }
}
