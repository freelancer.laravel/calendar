<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthRepository
{
    public function registerUser($request){
        return User::create(
            array_merge(
                $request->validated(),
                [
                    'role' => 'user',
                    'password' => Hash::make($request->password)
                ]
            )
        );
    }
}
