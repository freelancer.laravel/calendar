<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthenticateApi extends Middleware
{
    protected function Authenticate($request, array $guards)
    {
        $token = $request->bearerToken();

        if ($token){
            return;
        }else{
            $this->unauthenticated($request, $guards);
        }
    }
}
