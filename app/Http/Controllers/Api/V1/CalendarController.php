<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCalendarRequest;
use App\Http\Requests\UpdateCalendarRequest;
use App\Http\Resources\CalendarResource;
use App\Models\Calendar;
use App\Repositories\CalendarRepository;
use App\Traits\Time;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalendarController extends Controller
{
    use Time;
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request, CalendarRepository $calendarRepository): JsonResponse
    {
        $user = Auth::user();

        $calendars = $calendarRepository->get($user, $request);

        return response()->json([
            'status' => 'success',
            'data' => CalendarResource::collection($calendars),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCalendarRequest  $request
     * @return JsonResponse
     */
    public function store(StoreCalendarRequest $request, CalendarRepository $calendarRepository): JsonResponse
    {
        $user = Auth::user();

        $createdCalendar = $calendarRepository->create($user, $request);

        return response()->json([
            'status' => 'success',
            'data' => new CalendarResource($createdCalendar)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return JsonResponse
     */
    public function show(Calendar $calendar): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'data' => new CalendarResource($calendar)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return JsonResponse
     */
    public function edit(Calendar $calendar): JsonResponse
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCalendarRequest  $request
     * @param  \App\Models\Calendar  $calendar
     * @return JsonResponse
     */
    public function update(UpdateCalendarRequest $request, Calendar $calendar, CalendarRepository $calendarRepository): JsonResponse
    {
        $user = Auth::user();

        if (!Time::isIntervalHours($calendar->date_start, 3)){

            return response()->json([
                'status' => 'error',
                'message' => 'Recordings can only be updated before the beginning of which is more than three hours.'
            ]);
        }

        $calendarRepository->update($user, $request, $calendar);

        return response()->json([
            'status' => 'success',
            'data' => new CalendarResource($calendar)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return JsonResponse
     */
    public function destroy(Calendar $calendar): JsonResponse
    {
        if (!Time::isIntervalHours($calendar->date_start, 3)){

            return response()->json([
                'status' => 'error',
                'message' => 'Delete can only be updated before the beginning of which is more than three hours.'
            ]);

        }

        $calendar->delete();

        return response()->json([
            'status' => 'success',
            'data' => ''
        ]);
    }
}
