# Calendar REST API

## About

В качестве аутентификации использовать Bearer-токены. Доступные эндпойнты:

### GET /calendar
Возвращает все записи в интервале между параметрами date_start и date_end. Доступен только для аутентифицированных пользователей.
date_start и date_end должны быть вида "2022-06-10 13:50:00"

### POST /calendar
Добавление новой записи в календарь. Запись добавляется на конкретное время date и с конкретной длительностью в минутах duration.
Также у записи есть поля title (обязательное) и description (не обязательное).
Если запись добавляет аутентифицированный пользователь, то возможно наложение различных записей по времени (например, 13:00-14:30 и 14:00-15:00).
date_start - должны быть вида "2022-06-10 13:50:00"
date_end - рассчитывается автоматически учитывая duration

### PATCH /calendar/{id}
Обновление конкретной записи в календаре. Обновлять можно любые поля, но только у записей, до начала которых больше трех часов. Доступен только для аутентифицированных пользователей.

### SHOW /calendar/{id}
Показ конкретной записи в календаре. Доступен только для аутентифицированных пользователей.
### DELETE /calendar/{id}
Удаление конкретной записи в календаре. Удалять можно только записи до которых осталось больше 3 часов. Доступен только для аутентифицированных пользователей.


## Installation Steps
1. git clone https://gitlab.com/freelancer.laravel/calendar.git yourFolder
2. cd yourFolder
3. composer install
4. cp .env.example .env
5. open .env Add the DB Credentials & APP_URL
6. DB_HOST=localhost
   DB_DATABASE=homestead
   DB_USERNAME=homestead
   DB_PASSWORD=secret
7. cd yourFolder
8. php artisan config:cache
9. composer dump-autoload
10. php artisan key:generate
11. php artisan config:cache
12. php artisan migrate
13. php artisan passport:install
## License

The Calendar REST API is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
